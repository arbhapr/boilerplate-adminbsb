@extends('back.master')
@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <h2>Create Member</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <form action="{{route('member.store')}}" method="POST" autocomplete="off">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" required />
                                    <label class="form-label">Name</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group form-float">
                                <div class="">
                                    <select class="form-control show-tick" name="role" required>
                                        <option value="admin">Admin</option>
                                        <option value="member" selected>Member</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" required />
                                    <label class="form-label">E-Mail</label>
                                </div>
                                @error('email')
                                <small style="color:red;">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" name="password" class="form-control" required />
                                    <label class="form-label">Password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="submit" value="Submit" class="btn btn-success btn-lg">
                            <input type="reset" value="Reset" class="btn btn-warning btn-lg">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<a type="button" class="btn btn-warning btn-circle-lg waves-effect waves-circle waves-float"
    style="position:fixed; bottom:20px; right:20px;" href="{{route('member.index')}}">
    <i class="material-icons">undo</i>
</a>
@endsection