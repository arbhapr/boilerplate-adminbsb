@extends('back.master')
@section('content')
@if(Session('success'))
    <div class="alert alert-alert-dismissible alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{Session('success')}}
    </div>
@elseif(Session('danger'))
    <div class="alert alert-alert-dismissible alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{Session('danger')}}
    </div>
@endif
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <h2>Member List</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th width="10%">Role</th>
                                <th width="15%">Registered</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($members as $n => $member)
                            <tr>
                                <td>{{$n+1}}</td>
                                <td>{{$member->name}}</td>
                                <td>{{$member->email}}</td>
                                <td>{{ucwords($member->role)}}</td>
                                <td>{{date('M d, Y', strtotime($member->created_at))}}</td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-warning waves-effect" href="{{route('member.edit', $member->id)}}">
                                        <i class="material-icons">settings</i>
                                    </a>
                                    @if($member->id != Auth::id())
                                    <a class="btn btn-xs btn-danger waves-effect"
                                        href="{{route('member.destroy', $member->id)}}"
                                        onclick="return confirm('Are you sure want to delete this member?');">
                                        <i class="material-icons">delete</i>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<a type="button" class="btn btn-success btn-circle-lg waves-effect waves-circle waves-float"
    style="position:fixed; bottom:20px; right:20px;" href="{{route('member.create')}}">
    <i class="material-icons">add</i>
</a>
@endsection