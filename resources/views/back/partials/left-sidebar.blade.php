<aside id="leftsidebar" class="sidebar">
    <div class="user-info">
        <div class="row">
            <a href="">
                <div class="image col-xs-2">
                    <img src="{{asset('vendor/AdminBSB')}}/images/user.png" width="48" height="48" alt="User" />
                </div>
            </a>
            <div class="info-container col-xs-7">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <b>{{$loguser->name}}</b></div>
                <div class="email">{{$loguser->email}}</div>
            </div>
            <div class="info-container col-xs-2" style="top:20px;">
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" style="color:white;" title="Keluar">
                    <i class="material-icons">exit_to_app</i>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
    
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{(Request::segment(1) == 'dashboard')?'active':''}}">
                <a href="{{route('backend.index')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li class="{{(Request::segment(1) == 'master')?'active':''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">settings</i>
                    <span>Master Data</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{route('member.index')}}">
                            <span>Members</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    
    <div class="legal">
        <div class="copyright">
            &copy; {{date('Y')}} <a href="javascript:void(0);">{{ config('app.name', 'AdminBSB - Boilerplate') }}</a>.
        </div>
        <div class="version">
            <b>Version: </b> {{ env('APP_VERSION', 'Developing') }}
        </div>
    </div>
</aside>