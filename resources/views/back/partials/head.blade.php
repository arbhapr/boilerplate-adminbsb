<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="keywords" content="HTML,CSS,XML,JavaScript">
<meta name="author" content="{{ env('APP_CREATOR', 'CODEBREEDER') }}">
<title>{{ config('app.name', 'AdminBSB - Boilerplate') }}</title>
<link rel="icon" href="{{asset('vendor/AdminBSB')}}/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link href="{{asset('vendor/AdminBSB')}}/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="{{asset('vendor/AdminBSB')}}/plugins/node-waves/waves.css" rel="stylesheet" />
<link href="{{asset('vendor/AdminBSB')}}/plugins/animate-css/animate.css" rel="stylesheet" />
<link href="{{asset('vendor/AdminBSB')}}/plugins/morrisjs/morris.css" rel="stylesheet" />
<link href="{{asset('vendor/AdminBSB')}}/css/style.css" rel="stylesheet">
<link href="{{asset('vendor/AdminBSB')}}/css/themes/all-themes.css" rel="stylesheet" />

<link href="{{asset('vendor/AdminBSB')}}/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link href="{{asset('vendor/AdminBSB')}}/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
@yield('css')