<script src="{{asset('vendor/AdminBSB')}}/plugins/jquery/jquery.min.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/bootstrap/js/bootstrap.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/node-waves/waves.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/jquery-countto/jquery.countTo.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/raphael/raphael.min.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/morrisjs/morris.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/chartjs/Chart.bundle.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/flot-charts/jquery.flot.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/flot-charts/jquery.flot.time.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/jquery-sparkline/jquery.sparkline.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/js/admin.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/js/pages/index.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/js/demo.js"></script>

<script src="{{asset('vendor/AdminBSB')}}/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="{{asset('vendor/AdminBSB')}}/js/pages/tables/jquery-datatable.js"></script>

<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>

<script>
    CKEDITOR.replace('ckeditor');
    $('.dataTable').DataTable();
</script>

@yield('script')