﻿@php
    $loguser = App\User::find(Auth::id());
@endphp
<!DOCTYPE html>
<html>
    <head>
        @include('back.partials.head')
    </head>
    <body class="theme-red">
        @include('back.partials.loader')
        @include('back.partials.navbar')
        <section>
            @include('back.partials.left-sidebar')
            @include('back.partials.right-sidebar')
        </section>
        <section class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </section>
        @include('back.partials.script')
    </body>
</html>
