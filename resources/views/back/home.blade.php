@extends('back.master')
@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <h2>About {{ config('app.name', 'AdminBSB - Boilerplate') }}</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <p>
                    {{ config('app.name', 'AdminBSB - Boilerplate') }} is a simple application builded with AdminBSB Boilerplate by {{ env('APP_CREATOR', 'CODEBREEDER') }}.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>Recent Information</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th width="10%">Status</th>
                                <th>Message</th>
                                <th width="15%">Published</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="label bg-red">important</span></td>
                                <td>This is your first message</td>
                                <td>{{date('M d, Y')}}</td>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection