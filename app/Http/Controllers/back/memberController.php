<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class memberController extends Controller
{
    public function index()
    {
        $data['members'] = User::orderBy('role', 'ASC')->get(); 
        return view('back.member.index', $data);
    }

    public function create()
    {
        return view('back.member.create');
    }
    
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|unique:users',
        ]);

        $user = new User;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->role     = $request->role;
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('member.index')->with('success', 'Member has been added successfully!');
    }
    
    public function edit($id)
    {
        $data['member'] = User::find($id);
        return view('back.member.edit', $data);
    }
    
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'email' => 'required|unique:users,email,'.$id,
        ]);

        $user = User::find($id);
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->role     = $request->role;
        if($request->password){
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return redirect()->route('member.index')->with('success', 'Member has been updated successfully!');
    }
    
    public function destroy($id)
    {
        $user = User::find($id)->delete();
        return redirect()->route('member.index')->with('danger', 'Member has been deleted successfully!');
    }
}
