<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::redirect('/', '/dashboard');

Route::prefix('/dashboard')->middleware(['auth'])->name('backend.')->group( function(){
    Route::get('/', 'Back\mainController@index')->name('index');
});

Route::prefix('/master/member')->middleware(['auth', 'role:admin'])->name('member.')->group( function(){
    Route::get('/', 'Back\memberController@index')->name('index');
    Route::get('/create', 'Back\memberController@create')->name('create');
    Route::post('/store', 'Back\memberController@store')->name('store');
    Route::get('/edit/{id}', 'Back\memberController@edit')->name('edit');
    Route::post('/update/{id}', 'Back\memberController@update')->name('update');
    Route::get('/destroy/{id}', 'Back\memberController@destroy')->name('destroy');
});